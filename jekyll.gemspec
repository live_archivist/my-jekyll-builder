# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "my-jekyll-builder"
  spec.version       = "0.13"
  spec.authors       = ["wesdottoday"]
  spec.email         = ["wes.kennedy@protonmail.com"]

  spec.summary       = "A docker container for building Jekyll sites"
  spec.homepage      = "https://wes.today"
  spec.license       = "MIT"

  spec.add_runtime_dependency "jekyll", ">= 3.7.3"
  spec.add_runtime_dependency "jekyll-seo-tag", ">= 2.1.0"

  spec.add_development_dependency "bundler", "> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
