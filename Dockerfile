FROM jekyll/jekyll:builder

WORKDIR /app
COPY Gemfile /app/
COPY jekyll.gemspec /app/

RUN ["chown", "-R", "root:root", "/app"]
RUN ["chmod", "-R", "777", "/app"]
RUN ["bundle", "config", "set", "path", "."]
RUN ["bundle", "install"]

ENTRYPOINT ["/usr/jekyll/bin/entrypoint"]